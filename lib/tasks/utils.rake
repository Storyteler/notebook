# frozen_string_literal: true

namespace :utils do
  desc 'Popular banco de dados.'
  task seed: :environment do
    puts 'Gerando os contatos (Contacts)...'
    400.times do |_i|
      Contact.create!(
        name: Faker::Name.name,
        email: Faker::Internet.email,
        #rmk: Faker::OnePiece.quote,
        rmk: Faker::Lorem.paragraph([1,2,3].sample),
        kind: Kind.all.sample
      )
    end
    puts 'Gerando os contatos (Contacts)... [Ok]'

    puts 'Gerando os endereços (Addresses)...'
    Contact.all.each do |_contact|
      Address.create!(
        street: Faker::Address.street_address,
        city: Faker::Address.city,
        state: Faker::Address.state_abbr,
        contact: _contact
      )
    end
    puts 'Gerando os endereços (Addresses)... [Ok]'

    puts 'Gerando os telefones (Phones)...'
    Contact.all.each do |_contact|
      Random.rand(1..3).times do |_i|
        Phone.create!(
          phone: Faker::PhoneNumber.cell_phone,
          contact: _contact
        )
      end
    end
    puts 'Gerando os telefones (Phones)... [Ok]'
  end
end
