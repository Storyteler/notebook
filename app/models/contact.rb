# frozen_string_literal: true

class Contact < ApplicationRecord
  belongs_to :kind
  has_one :address, dependent: :destroy
  has_many :phones

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :phones, reject_if: :all_blank, allow_destroy: true
end
